from jrnl import install, Journal
from jrnl.args import parse_args
from jrnl.config import load_config, get_journal_name, scope_config
from jrnl.Journal import open_journal


def open_jrnl(jrnl: str = "default") -> Journal:
    # A bit of a magic invocation to get a jrnl config object that we can use. Since
    # this uses internal APIs it is liable to break at any time.
    # This was reverse-engineered form the jrnl source code.
    jrnl_config = load_config(install.find_default_config())
    jrnl_args = parse_args()
    jrnl_args = get_journal_name(jrnl_args, jrnl_config)
    jrnl_config = scope_config(jrnl_config, jrnl_args.journal_name)

    return open_journal("default", jrnl_config)
