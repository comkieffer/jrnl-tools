#!/usr/bin/env python3

from datetime import datetime
import sys

from http.server import HTTPServer, SimpleHTTPRequestHandler

import click
from click import Context

import humanize
import jinja2
import markdown

# Todo: should just import jrnl
from jrnl import install
from jrnl.args import parse_args
from jrnl.config import load_config, get_journal_name, scope_config
from jrnl.Journal import open_journal


class Handler(SimpleHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, directory="jrnl-web/static/", **kwargs)


def markdown_filter(md: str) -> str:
    return markdown.markdown(md)


def humanize_filter(value) -> str:
    if isinstance(value, datetime):
        return humanize.naturaldate(value)

    return value


@click.group()
@click.pass_context
def main(ctx: Context) -> None:
    ctx.ensure_object(dict)


@main.command()
@click.pass_context
def serve(ctx: Context) -> None:
    server_address = ("localhost", 8000)
    click.echo(f"Serving journal contents on {server_address[0]}:{server_address[1]}.")
    click.echo("Press Ctrl-C to stop.")

    httpd = HTTPServer(server_address, Handler)
    httpd.serve_forever()


@main.command()
@click.pass_context
def build(ctx: Context) -> None:
    # A bit of a magic invocation to get a jrnl config object that we can use
    # If we wanted to, we could pass in a jrnl command line to filter the list journals
    # we want to transform.
    jrnl_config = load_config(install.find_default_config())
    jrnl_args = parse_args()
    jrnl_args = get_journal_name(jrnl_args, jrnl_config)
    jrnl_config = scope_config(jrnl_config, jrnl_args.journal_name)

    jrnl = open_journal("default", jrnl_config)

    # Use .local/share/jrnl path for templates
    jinja_env = jinja2.Environment(
        loader=jinja2.FileSystemLoader("jrnl-web/templates/")
    )
    jinja_env.filters["markdown"] = markdown_filter
    jinja_env.filters["humanize"] = humanize_filter
    template = jinja_env.get_template("index.html")

    print(jrnl.entries[0].__dict__)

    jrnl.entries.reverse()
    out = template.render(entries=jrnl.entries)
    with open("jrnl-web/static/index.html", "w+") as f:
        f.write(out)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass

    sys.exit(0)
