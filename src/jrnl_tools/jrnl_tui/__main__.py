import sys

from .tui import main

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass

    sys.exit(0)
