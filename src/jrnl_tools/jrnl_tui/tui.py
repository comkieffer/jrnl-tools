from jrnl import Journal

from rich.markdown import Markdown

from textual.app import App, ComposeResult
from textual.containers import Horizontal, VerticalScroll
from textual.widgets import Header, Footer, Label, ListItem, ListView, Static, Markdown

from jrnl import Entry
from jrnl_tools.common import open_jrnl

# TODO: Properly truncate titles in list
# TODO: Make it pretty
# TODO: Add tags pane (filter entries based on tags
# TODO: Add delete and edit actions (for the current note)
# TODO: Add search option (to filter the list of entries)
# TODO: For narrow screens, collapse the list of notes


class JrnlEntryListItem(ListItem):
    def __init__(self, entry: Entry, *args, **kwargs):
        self.entry = entry

        super().__init__(
            Horizontal(
                Label(
                    (
                        f" {entry.date.strftime('%d')} \n"
                        f" {entry.date.strftime('%b')} \n"
                    ),
                    id="entry_date",
                ),
                Label(f"[bold]{entry.title}[/bold]\n", id="entry_title"),
            ),
            *args,
            **kwargs,
        )


class JrnlTuiApp(App):
    """A Textual app to manage stopwatches."""

    CSS_PATH = "styles/jrnl_tui_app.css"

    BINDINGS = [
        ("e", "edit_entry", "Edit Current Entry"),
        ("n", "new_entry", "Create New Entry"),
        ("j", "previous_entry", "Previous Entry"),
        ("k", "next_entry", "Next Entry"),
        ("d", "toggle_dark", "Toggle dark mode"),
    ]

    def __init__(self) -> None:
        super().__init__()
        self.journal = open_jrnl()

    def compose(self) -> ComposeResult:
        """Create child widgets for the app."""
        yield Header()

        jrnl_entry_items = [JrnlEntryListItem(entry) for entry in self.journal.entries]
        jrnl_entry_items.reverse()
        with Horizontal(id="body"):
            yield ListView(*jrnl_entry_items, id="sidebar")
            with VerticalScroll(id="main-pane"):
                yield Static(id="entry-tag-list", markup=True)
                yield Markdown(id="entry-contents")

        yield Footer()

    def on_mount(self) -> None:
        self.screen.set_focus(self.query_one("#sidebar"))

    def on_list_view_highlighted(self, message) -> None:
        if message.item is None:
            return
        entry = message.item.entry
        entry_body_cnt = self.screen.query_one("#entry-contents")
        entry_body_cnt.update(entry.body)
        entry_tags_cnt = self.screen.query_one("#entry-tag-list")
        entry_tags_cnt.update(f"Tags: {', '.join(entry.tags)}")

    def action_toggle_dark(self) -> None:
        self.dark = not self.dark

    def edit_entry(self) -> None:
        pass

    def action_previous_entry(self):
        entry_list_view = self.screen.query_one("#sidebar")
        entry_list_view.index -= 1

    def action_next_entry(self):
        entry_list_view = self.screen.query_one("#sidebar")
        entry_list_view.index += 1


def main() -> None:
    app = JrnlTuiApp()
    # Doesn't actually seem to hand event to the list items. Maybe we need to focus
    # those instead.

    app.run()
