#! /usr/bin/env python3

import sys

import click
from click import Context


@click.group()
@click.pass_context
def main(ctx: Context) -> None:
    ctx.ensure_object(dict)


@main.command()
@click.pass_context
def repl(ctx: Context) -> None:
    pass


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass

    click.echo("Exiting gracefully ...")
    sys.exit(0)
