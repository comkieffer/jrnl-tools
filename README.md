# Jrnl Tools 

## Install 

Install `jrnl` manually from pip since `jrnl` and `textual` want different versions for `rich`. 

Download a release with 

``` commandline 
pipx install git+https://gitlab.com/comkieffer/jrnl-tools.git@TAG
pipx inject jrnl-tools jrnl
```

where `TAG` is the git tag that will be installed. 
